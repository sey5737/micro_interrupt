uint32_t current_micro = 0;
uint32_t pri_micro = 0;
uint16_t sec = 0;
uint32_t rising_time = 0;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(2, INPUT);
  attachInterrupt(2, state_change, RISING);
}

void loop() {
  // put your main code here, to run repeatedly:
  current_micro = micros();
  
  sec = (current_micro - pri_micro) / 1000000;
  }
 void state_change(){
  if(sec>=5){
    rising_time = current_micro - pri_micro;
    Serial.print("sec : ");
    Serial.println(sec);
    sec=0;
    Serial.print("rising_time : ");
    Serial.println(rising_time);
    pri_micro = current_micro; 
  }
 }
